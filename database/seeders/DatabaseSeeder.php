<?php

namespace Database\Seeders;

use App\Models\HealthcareProvider;
use App\Models\Type;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        HealthcareProvider::factory()->has(
            Type::factory()
        )
            ->count(20)
            ->create();
    }
}
