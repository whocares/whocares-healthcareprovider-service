<?php

namespace Database\Factories;

use App\Models\HealthcareProvider;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

class HealthcareProviderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HealthcareProvider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'type_id' => Type::factory()
        ];
    }
}
