<?php

namespace Tests\Feature;

use App\Firebase\User;
use App\Models\HealthcareProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class HealthcareproviderControllerTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetHealthcareprovidersOfPatient() {
        $userId = '12345';
        $healthcareprovider1 = HealthcareProvider::factory()->create();
        $healthcareprovider2 = HealthcareProvider::factory()->create();
        DB::insert('insert into healthcareprovider_patient(healthcareprovider_id, patient_id) values(?, ?)', [$healthcareprovider1->id, $userId]);
        DB::insert('insert into healthcareprovider_patient(healthcareprovider_id, patient_id) values(?, ?)', [$healthcareprovider2->id, $userId]);

        $userMock = $this->createMock(User::class);
        $userMock->method('getId')->willReturn($userId);
        Auth::partialMock()->expects('user')->andReturn($userMock);

        $response = $this->get('/api/v1/hcp-service/healthcare-providers');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'healthcareProviders' => [
                '*' => [
                    'id', 'first_name', 'last_name', 'type' => ['name', 'description']
                ]
            ]
        ]);
    }

    public function testGetByUserId() {
        $userId = '12345';
        $healthcareprovider1 = HealthcareProvider::factory()->create();
        DB::update('update healthcareproviders SET firebase_user_id = ? where id = ?', [$userId, $healthcareprovider1->id]);

        $response = $this->get('/api/v1/hcp-service/healthcare-providers/user/' . $userId);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'healthcareProvider' => [
                'id', 'first_name', 'last_name', 'type' => ['name', 'description']
            ]
        ]);
    }
}
