<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use App\Models\Type;

class HealthcareProvider extends Model
{
    use HasFactory;

    protected $table = 'healthcareproviders';

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'type_id',
        'firebase_user_id'
    ];

    public function type() {
        return $this->belongsTo(Type::class);
    }
}
