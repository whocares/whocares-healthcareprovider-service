<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HealthcareproviderCollection;
use App\Http\Resources\HealthcareProviderResource;
use App\Models\HealthcareProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HealthcareProviderController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @return HealthcareproviderCollection
     */
    public function index(Request $request)
    {
        $patient_id = Auth::user()->getId();

        $healthcareproviderIds = DB::table('healthcareproviders')
            ->select('healthcareproviders.id')
            ->join('healthcareprovider_patient',
            'healthcareproviders.id', '=',
            'healthcareprovider_patient.healthcareprovider_id')
            ->where('healthcareprovider_patient.patient_id','=', $patient_id)
            ->get();

        $healthcareproviders = HealthcareProvider::query()
            ->whereIn('id', $healthcareproviderIds->pluck('id'))
            ->get();

        HealthcareproviderCollection::wrap('healthcareProviders');
        return new HealthcareproviderCollection($healthcareproviders);
    }

    /**
     * Get a healthcare provider by its user ID.
     *
     * @param  String  $userID
     * @return HealthcareProviderResource
     */
    public function getByUserId(String $userID): HealthcareProviderResource
    {
        $hcp = HealthcareProvider::where([
            'firebase_user_id' => $userID
        ])->first();

        HealthcareProviderResource::wrap('healthcareProvider');
        return new HealthcareProviderResource($hcp);
    }
}
