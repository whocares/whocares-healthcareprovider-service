<?php

namespace App\Firebase;

use Exception;
use Firebase\Auth\Token\Verifier;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Lcobucci\JWT\Parser;

class FirebaseGuard implements Guard
{
    use GuardHelpers;

    /**
     * @var Verifier instance.
     */
    protected $verifier;

    /**
     * Guard constructor.
     */
    public function __construct()
    {
        $host = request()->getHttpHost();
        $projectID = null;

        if ($host == config('services.base_hosts.patient')) {
            $projectID = config('services.firebase.patient_project_id');
        } elseif ($host == config('services.base_hosts.hcp')) {
            $projectID = config('services.firebase.hcp_project_id');
        }

        $this->verifier = new Verifier($projectID);
    }

    /**
     * @inheritDoc
     */
    public function user(): ?User
    {
        $token = (new Parser())
            ->parse(
                request()->bearerToken()
            );

        try {
            $token = $this->verifier->verifyIdToken($token);
            return new User($token->getClaims());
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function validate(array $credentials = []): bool
    {
        return false;
    }
}
